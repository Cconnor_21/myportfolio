<style>
html {
    overflow: scroll;
    overflow-x: hidden;
}
::-webkit-scrollbar {
    width: 0px;  /* Remove scrollbar space */
    background: transparent;  /* Optional: just make scrollbar invisible */
}
/* Optional: show position indicator in red */
::-webkit-scrollbar-thumb {
    background: #FF0000;
}
body {
  background: url('images/secondimage.jpg') no-repeat center center/cover;
}

.card-container {
  margin-top:100px;
  display: flex;
  justify-content: center;
  background-color: transparent;
  flex-wrap: wrap;

}

.card{
  height:340px;
  width:450px;
  background-color: rgba(255, 255, 255, .5);
  position:relative;
  margin:10px;
  border-radius:10px;
  color: white;
}

.bottomBar{
  position:absolute;
  background-color: rgba(255, 255, 255, 1);
  width:100%;
  height:60px;
  bottom:0px;
  display:flex;
  justify-content:center;
  align-items:center;
  border-radius: 0px 0px 10px 10px;
  border-top: 1px solid white;
  color:rgba(20,146,202,1);
}

.topBar{
  position:absolute;
  background-color: rgba(20,146,202,1);
  width:100%;
  height:40px;
  top:0px;
  display:flex;
  justify-content:center;
  align-items:center;
  border-radius: 10px 10px 0px 0px;
}

.label{
  background-color:transparent;
  width:60%;
  text-align:center;
  padding:5px;
  font-size:25px;
}

.button {
  background-color:transparent;
  width:60%;
  text-align:center;
  padding:5px;
  font-size:20px;
  border-radius:5px;
  border:2px solid rgba(20,146,202,1);
  cursor:pointer;
}

.button:hover {
  background-color:rgba(20,146,202,1);
  transition: 0.3s;
  color:white;
  width:60%;
  text-align:center;
  padding:5px;
  font-size:20px;
  border-radius:5px;
  border:2px solid rgba(20,146,202,1);
  cursor:pointer;
}

.image-container{
  position:absolute;
  border:1px solid
  background-color:transparent;
  width:100%;
  height:250px;
  top:10%;
  display:flex;
  justify-content:center;
  align-items:center;
}
</style>
<div class="body-container">
<div class="card-container">
  <div class="card">
  <div class="topBar">
  <div class="label">Capstone Project</div>
  </div>

  <div class="image-container">
    <img src="images/creativecybersolutions.png" style="width:100%;">
  </div>

  <div class="bottomBar">
    <div class="button">View Project</div>
    </div>
  </div>

  <div class="card">
  <div class="topBar">
  <div class="label">Weather Api</div>
  </div>

  <div class="image-container">
    <img src="images/weatherapi.png" style="width:100%;">
  </div>

  <div class="bottomBar">
    <div class="button">View Project</div>
    </div>
  </div>

  <div class="card">
  <div class="topBar">
  <div class="label">Grocery List</div>
  </div>

  <div class="image-container">
    <img src="images/grocerylist.png" style="width:100%;">
  </div>

  <div class="bottomBar">
    <div class="button">View Project</div>
    </div>
  </div>

  <div class="card">
  <div class="topBar">
  <div class="label">Grocery List</div>
  </div>

  <div class="image-container">
    <img src="images/flashcards.png" style="width:100%;">
  </div>

  <div class="bottomBar">
    <div class="button">View Project</div>
    </div>
  </div>

  <div class="card">
  <div class="topBar">
  <div class="label">Grocery List</div>
  </div>

  <div class="image-container">
    <img src="images/bootstraptheme.png" style="width:100%;">
  </div>

  <div class="bottomBar">
    <div class="button">View Project</div>
    </div>
  </div>

  <div class="card">
  <div class="topBar">
  <div class="label">Grocery List</div>
  </div>

  <div class="image-container">
    <img src="images/bootstraptheme.png" style="width:100%;">
  </div>

  <div class="bottomBar">
    <div class="button">View Project</div>
    </div>
  </div>
</div>
</div>
