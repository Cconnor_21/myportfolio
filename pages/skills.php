<style>
html {
    overflow: scroll;
    overflow-x: hidden;
}
::-webkit-scrollbar {
    width: 0px;  /* Remove scrollbar space */
    background: transparent;  /* Optional: just make scrollbar invisible */
}
/* Optional: show position indicator in red */
::-webkit-scrollbar-thumb {
    background: #FF0000;
}
body {

  background: url('images/thirdimage.jpg') no-repeat center center/cover;
}

.main-container{
  display:flex;
  align-items:center;
  justify-content: center;
  margin-top:100px;
}

.skills-container{
  width:70%;
  background-color: rgba(255, 255, 255, .1);
  padding:40px;
  border-radius:20px;
  color:white;

}

.row {
  width:100%;
}

.column, .filler-column {
  flex-basis: 100%;
}

@media screen and (min-width: 800px) {


  .row {
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
  }
  .column {
    flex: 1;
  }
  .filler-column {
    flex: 1;
    background:transparent;
  }
  ._25 {
    flex: 2.5;
  }
  ._5 {
    flex: 5;
  }
}

@media screen and (max-width: 800px) {
  .skills-container{
    width:100%;
    background-color: rgba(255, 255, 255, .1);
    padding:10px;
    border-radius:0px;
    color:white;

  }
  .filler-column {
    flex: 1;
    display:none;
  }

  .row {
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
  }

  .row {
    width:100%;
  }

}

.filler-column {
  text-align:center;
  padding: 15px;
  border: 1px solid transparent;
  margin: 5px 7px;
}

.column {
  text-align:center;
  color:black;
  padding: 15px;
  border: 1px solid #666;
  margin: 5px 7px;
  background: white;
}

.center-text {
  align:center;
  padding-right:30px;
  padding-left:30px;
  text-align:center;
}

</style>
<div class="main-container">
<div class="skills-container">
              <div class="center-text">
              <h1 style="font-size:40px;">My Skills</h1>
              <hr />
            </div>
              <h2 class="center-text">Programming Languages</h2>
              <div class="row">
                <div class="column">
                  C#
                </div>
                <div class="column">
                  HTML/HTML5
                </div>
                <div class="column">
                  CSS
                </div>
              </div>

              <div class="row">
                <div class="column">
                  C#
                </div>
                <div class="column">
                  HTML/HTML5
                </div>
                <div class="filler-column">
                </div>
              </div>

              <h2 class="center-text">Frameworks</h2>
              <div class="row">
                <div class="column">
                  C#
                </div>
                <div class="column">
                  HTML/HTML5
                </div>
                <div class="column">
                  CSS
                </div>
              </div>

              <div class="row">
                <div class="column">
                  C#
                </div>
                <div class="column">
                  HTML/HTML5
                </div>
                <div class="filler-column">
                </div>
              </div>

              <h2 class="center-text">Databases</h2>
              <div class="row">
                <div class="column">
                  C#
                </div>
                <div class="column">
                  HTML/HTML5
                </div>
                <div class="column">
                  CSS
                </div>
              </div>

              <div class="row">
                <div class="column">
                  C#
                </div>
                <div class="filler-column">
                </div>
                <div class="filler-column">
                </div>
              </div>

              <h2 class="center-text">Version Control</h2>
              <div class="row">
                <div class="column">
                  C#
                </div>
                <div class="column">
                  HTML/HTML5
                </div>
                <div class="filler-column">
                </div>
              </div>

              <h2 class="center-text">Exposure To</h2>
              <div class="row">
                <div class="column">
                  C#
                </div>
                <div class="column">
                  HTML/HTML5
                </div>
                <div class="filler-column">
                </div>
              </div>
            </div>
</div>
