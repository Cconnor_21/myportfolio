<style>


.main-container{
  background: url('images/secondimage.jpg') no-repeat center center/cover;
  margin:0px;
  padding:0px;

  overflow:hidden;
}

.flex-container{
  margin-top:50px;
}

.cropcircle{
    width: 210px;
    height: 210px;
    border-radius: 100%;
    border: 7px solid white;
    background: #eee no-repeat center;
    background-size: cover;
}

#image1{
    background-image: url(images/profile.jpg);
    background-position: 0px -1px;
}

.form-layout{
  background-color:white;
  height:400px;
}

input[type=text] {
  width: 100%;
  box-sizing: border-box;
  border: none;
  background-color: rgba(20,146,202,1);
  color: white;
  padding:10px;
  font-size:20px;
  border-radius:5px;
}

textarea {
  width: 100%;
  height:100px;
  box-sizing: border-box;
  border: none;
  background-color: rgba(20,146,202,1);
  color: white;
  resize:none;
  padding:10px;
  font-size:20px;
  border-radius:5px;
}

label{
  font-size:25px;
  padding:0px;
}

.submit {
  background-color: transparent;
  border:2px solid rgba(20,146,202,1);
  color: rgba(20,146,202,1);
  width:30%;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin-top:20px;
  cursor: pointer;
  border-radius:5px;
}

.submit:hover {
  background-color: rgba(20,146,202,1);
  border:2px solid rgba(20,146,202,1);
  color: white;
  width:30%;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin-top:20px;
  cursor: pointer;
  border-radius:5px;
}

</style>
<!-- <img src="images/profile.png" style="width:300px;"> -->
<div class="main-container">
<div class="flex-container">

  <div class="about-content" style="">
    <h1 style=" margin-bottom:10px;">Contact Me</h1>
    <hr style=" text-align:center;" />
    <form>
  <label for="fname">Name</label>
  <input type="text" id="fname" name="fname">
  <label for="lname">Email</label>
  <input type="text" id="lname" name="lname">
  <label for="lname">Message</label>
  <textarea></textarea>

</form>
<button class="submit">Send Message</button>
  </div>
</div>
</div>
