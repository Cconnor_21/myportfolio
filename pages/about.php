<style>


.main-container{
  background: url('images/secondimage.jpg') no-repeat center center/cover;
  margin:0px;
  padding:0px;
  overflow:hidden;
}

.cropcircle{
    width: 210px;
    height: 210px;
    border-radius: 100%;
    border: 7px solid white;
    background: #eee no-repeat center;
    background-size: cover;
}

#image1{
    background-image: url(images/profile.jpg);
    background-position: 0px -1px;
}
</style>
<!-- <img src="images/profile.png" style="width:300px;"> -->
<div class="main-container">
<div class="flex-container">

  <div class="about-content animated zoomIn faster" style="">
    <div class="image-container">
    <div class="cropcircle animated zoomIn faster" id="image1"></div>
  </div>

    <h1>Cody Connor<h1>
      <h2>Software Developer</h2>
      <div class="limit">
      <p >Lorem ipsum dolor sit amet, consectetur adipiscing elit. In in eros vitae ante aliquam euismod vel vel ipsum. Morbi accumsan elementum orci nec faucibus. Nulla accumsan velit eget libero ullamcorper, aliquet placerat dui sollicitudin. Maecenas scelerisque, lacus vel eleifend dictum, mauris tellus tempus erat, euismod sodales sapien nisl ut libero. Nam tristique felis nec rutrum blandit. Aenean quis condimentum libero. Suspendisse rhoncus odio sed turpis ullamcorper bibendum. </p>
      </div>
  </div>
</div>
</div>
