<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
<link href="css/main.css" rel="stylesheet">


<style>


</style>
<?php

  ?>
  <nav>
   <div class="brand">Cody Connor</div>
     <ul>
       <li class="list-item" id="about"><a href="#about">About</a></li>
       <li class="list-item" id="skills"><a href="#skills">Skills</a></li>
       <li class="list-item" id="projects"><a href="#projects">Projects</a></li>
       <li class="list-item" id="contact"><a href="#Contact">Contact</a></li>
     </ul>
     <div class="icons animated">
       <a><i class="fab fa-gitlab"></i></a>
       <a><i class="fab fa-github"></i></a>
       <a href="#" id="toggle"><i id="icon" class="fas fa-bars"></i>      </a>
     </div>
   </nav>

  <?php

  echo "

  <div id='content'></div>

  ";

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>

$(function() {
  var listitems = $('.list-item a')
  $('nav ul').append('<ol></ol>');
  $('nav').each(function() {
    for (var i = 0; i <= listitems.length - 1; i++) {
      $('nav ul > ol').append("<li>" + i + "</li>");
      $('nav ul > ol > li').eq(i).html(listitems.eq(i).html());
    }
  });
  $('#toggle').on('click', function() {
    $('ul').children('ol').slideToggle(200);
    //$('#icon').toggleClass('fab fa-times');

  });
});

  function navActive(){
    $(".test>li>a").removeClass("link-active active");
    $(".test>li>a").addClass("link");
  }

  window.onbeforeunload = function() {
  localStorage.removeItem(key);
  return 'about';
};

  $( document ).ready(function() {

  $("#about").click(function(){
    //localStorage.setItem("state", "about");
    navActive();
    $("#content").load("pages/about.php");
    $(this).removeClass("link");
    $(this).addClass("link-active active");
    $('.navbar-collapse').removeClass('show');
  });
  $("#skills").click(function(){
    //localStorage.setItem("state", "skills");
    navActive();
    $("#content").load("pages/skills.php");
    $(this).removeClass("link");
    $(this).addClass("link-active active");
    $('.navbar-collapse').removeClass('show');
  });
  $("#projects").click(function(){
    //localStorage.setItem("state", "projects");
    navActive();
    $("#content").load("pages/projects.php");
    $(this).removeClass("link");
    $(this).addClass("link-active active");
    $('.navbar-collapse').removeClass('show');
  });
  $("#contact").click(function(){
    //localStorage.setItem("state", "contact");
    navActive();
    $("#content").load("pages/contact.php");
    $(this).removeClass("link");
    $(this).addClass("link-active active");
    $('.navbar-collapse').removeClass('show');
  });

  $("#about").removeClass("link");
  $("#about").addClass("link-active active");
  console.log(localStorage.getItem("state"));
  $("#content").load("pages/about.php");

});
</script>
